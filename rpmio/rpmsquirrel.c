#include "system.h"
#include <stdarg.h>

#include <argv.h>

#if defined(MODULE_EMBED)
#include <squirrel.h>
#undef WITH_SQUIRREL
#endif

#if defined(WITH_SQUIRREL)
#include <dlfcn.h>
#include <rpmlog.h>
#endif

#define _RPMSQUIRREL_INTERNAL
#include "rpmsquirrel.h"

#include "debug.h"

/*@unchecked@*/
int _rpmsquirrel_debug = 0;

/*@unchecked@*/ /*@relnull@*/
rpmsquirrel _rpmsquirrelI = NULL;

#if defined(WITH_SQUIRREL)
static int dlopened = 0;
static rpmsquirrel (*rpmsquirrelNew_p) (char ** av, uint32_t flags);
static rpmRC (*rpmsquirrelRunFile_p) (rpmsquirrel squirrel, const char * fn, const char ** resultp);
static rpmRC (*rpmsquirrelRun_p) (rpmsquirrel squirrel, const char * str, const char ** resultp);
#endif

static void rpmsquirrelFini(void * _squirrel)
        /*@globals fileSystem @*/
        /*@modifies *_squirrel, fileSystem @*/
{
    rpmsquirrel squirrel = (rpmsquirrel) _squirrel;

#if defined(MODULE_EMBED)
    sq_close((HSQUIRRELVM)squirrel->I);
#endif
    squirrel->I = NULL;
    (void)rpmiobFree(squirrel->iob);
    squirrel->iob = NULL;
}

/*@unchecked@*/ /*@only@*/ /*@null@*/
rpmioPool _rpmsquirrelPool;

static rpmsquirrel rpmsquirrelGetPool(/*@null@*/ rpmioPool pool)
        /*@globals _rpmsquirrelPool, fileSystem @*/
        /*@modifies pool, _rpmsquirrelPool, fileSystem @*/
{
    rpmsquirrel squirrel;

    if (_rpmsquirrelPool == NULL) {
        _rpmsquirrelPool = rpmioNewPool("squirrel", sizeof(*squirrel), -1, _rpmsquirrel_debug,
                        NULL, NULL, rpmsquirrelFini);
        pool = _rpmsquirrelPool;
    }
    return (rpmsquirrel) rpmioGetPool(pool, sizeof(*squirrel));
}

#if defined(MODULE_EMBED)
static void rpmsquirrelPrint(HSQUIRRELVM v, const SQChar *s, ...)
{
    rpmsquirrel squirrel = sq_getforeignptr(v);
    size_t nb = 1024;
    char * b = xmalloc(nb);
    va_list va;

    va_start(va, s);
    while(1) {
	int nw = vsnprintf(b, nb, s, va);
	if (nw > -1 && (size_t)nw < nb)
	    break;
	if (nw > -1)		/* glibc 2.1 (and later) */
	    nb = nw+1;
	else			/* glibc 2.0 */
	    nb *= 2;
	b = xrealloc(b, nb);
    }
    va_end(va);

    (void) rpmiobAppend(squirrel->iob, b, 0);
    b = _free(b);
}

#if defined(SQUIRREL_VERSION_NUMBER) && SQUIRREL_VERSION_NUMBER >= 300
static void rpmsquirrelStderr(HSQUIRRELVM v, const SQChar *s,...)
{
    va_list vl;
    va_start(vl, s);
    vfprintf(stderr, s, vl);
    va_end(vl);
}
#endif
#endif

/* XXX FIXME: honor 0x8000000 in flags to use global interpreter */
static rpmsquirrel rpmsquirrelI(void)
	/*@globals _rpmsquirrelI @*/
	/*@modifies _rpmsquirrelI @*/
{
    if (_rpmsquirrelI == NULL)
	_rpmsquirrelI = rpmsquirrelNew(NULL, 0);
    return _rpmsquirrelI;
}

#if defined(WITH_SQUIRREL)
static void loadModule(void) {
    const char librpmsquirrel[] = "rpmsquirrel.so";
    void *h;

    h = dlopen (librpmsquirrel, RTLD_NOW|RTLD_GLOBAL);
    if (!h)
    {
	rpmlog(RPMLOG_WARNING, D_("Unable to open \"%s\" (%s), "
		    "embedded squirrel will not be available\n"),
		librpmsquirrel, dlerror());
    }
    else if(!((rpmsquirrelNew_p = dlsym(h, "rpmsquirrelNew"))
		&& (rpmsquirrelRunFile_p = dlsym(h, "rpmsquirrelRunFile"))
		&& (rpmsquirrelRun_p = dlsym(h, "rpmsquirrelRun")))) {
	rpmlog(RPMLOG_WARNING, D_("Opened library \"%s\" is incompatible (%s), "
		    "embedded squirrel will not be available\n"),
		librpmsquirrel, dlerror());
	if (dlclose (h))
	    rpmlog(RPMLOG_WARNING, "Error closing library \"%s\": %s", librpmsquirrel,
		    dlerror());
    } else
	dlopened = 1;
}
#endif

rpmsquirrel rpmsquirrelNew(char ** av, uint32_t flags)
{
#if defined(WITH_SQUIRREL)
    if (!dlopened) loadModule();
    if (dlopened) return rpmsquirrelNew_p(av, flags);
#endif
    rpmsquirrel squirrel = (flags & 0x80000000)
	? rpmsquirrelI() : rpmsquirrelGetPool(_rpmsquirrelPool);

#if defined(MODULE_EMBED)
    static const char * _av[] = { "rpmsquirrel", NULL };
    SQInteger stacksize = 1024;
    HSQUIRRELVM v = sq_open(stacksize);
    int ac;

    if (av == NULL) av = _av;
    ac = argvCount(av);

    squirrel->I = v;
    sq_setforeignptr(v, squirrel);
#if defined(SQUIRREL_VERSION_NUMBER) && SQUIRREL_VERSION_NUMBER >= 300
    sq_setprintfunc(v, rpmsquirrelPrint, rpmsquirrelStderr);
#else
    sq_setprintfunc(v, rpmsquirrelPrint);
#endif

#ifdef	NOTYET
    {	int i;
	sq_pushroottable(v);
	sc_pushstring(v, "ARGS", -1);
	sq_newarray(v, 0);
	for (i = 0, i < ac; i++) {
	    sq_pushstring(v, av[i], -1);
	    sq_arrayappend(v, -2);
	}
	sq_createslot(v, -3);
	sq_pop(v, 1);
    }
#endif
#endif
#if !defined(WITH_SQUIRREL)
    squirrel->iob = rpmiobNew(0);
#endif

    return rpmsquirrelLink(squirrel);
}

rpmRC rpmsquirrelRunFile(rpmsquirrel squirrel, const char * fn, const char ** resultp)
{
#if defined(WITH_SQUIRREL)
    if (dlopened) return rpmsquirrelRunFile_p(squirrel, fn, resultp);
#endif
    rpmRC rc = RPMRC_FAIL;

if (_rpmsquirrel_debug)
fprintf(stderr, "==> %s(%p,%s)\n", __FUNCTION__, squirrel, fn);

    if (squirrel == NULL) squirrel = rpmsquirrelI();

#if defined(NOTYET)
    if (fn != NULL && Tcl_EvalFile((Tcl_Interp *)squirrel->I, fn) == SQUIRREL_OK) {
	rc = RPMRC_OK;
	if (resultp)
	    *resultp = rpmiobStr(squirrel->iob);
    }
#endif
    return rc;
}

rpmRC rpmsquirrelRun(rpmsquirrel squirrel, const char * str, const char ** resultp)
{
#if defined(WITH_SQUIRREL)
    if (dlopened) return rpmsquirrelRun_p(squirrel, str, resultp);
#endif

    rpmRC rc = RPMRC_FAIL;

if (_rpmsquirrel_debug)
fprintf(stderr, "==> %s(%p,%s)\n", __FUNCTION__, squirrel, str);

    if (squirrel == NULL) squirrel = rpmsquirrelI();

#if defined(MODULE_EMBED)
    if (str != NULL) {
	size_t ns = strlen(str);
	if (ns > 0) {
	    HSQUIRRELVM v = squirrel->I;
	    SQBool raise = SQFalse;
	    SQInteger oldtop = sq_gettop(v);
	    SQRESULT res = sq_compilebuffer(v, str, ns, __FUNCTION__, raise);

	    if (SQ_SUCCEEDED(res)) {
		SQInteger retval = 0;
		sq_pushroottable(v);
		res = sq_call(v, 1, retval, raise);
	    }

	    sq_settop(v, oldtop);
	}
	rc = RPMRC_OK;
	if (resultp)
	    *resultp = rpmiobStr(squirrel->iob);
    }
#endif
    return rc;
}
